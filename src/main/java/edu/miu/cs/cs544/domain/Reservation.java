package edu.miu.cs.cs544.domain;

import java.util.List;

import jakarta.persistence.*;
import lombok.Data;

@Entity
@Data
public class Reservation {
    @Id
    @GeneratedValue
    private Integer id;
    @ManyToOne
    @JoinColumn(name = "customerId")
    private Customer customer;

    @Enumerated
    private ReservationState state;
    @OneToMany(mappedBy = "order")
    private List<Item> items;
    @Embedded
    private AuditData auditData;

}
